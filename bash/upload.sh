path=$1
directory=$2
newFile=$3

file=$(ls $path/*.jar)

if [[ ! -z $file ]] ;
then
  echo "File Found"

	ps -ef | grep $file | grep -v grep | awk '{print $2}' | xargs sudo kill -9

  rm $file

  mv $path$directory$newFile $path

	sudo nohup java -jar $path/$newFile  > $path/log.txt 2>&1 &

	echo $path$directory$newFile
else
	echo "File Not Found"

	mv $path$directory$newFile $path

	sudo nohup java -jar $path/$newFile  > $path/log.txt 2>&1 &

	echo $path$directory$newFile
fi