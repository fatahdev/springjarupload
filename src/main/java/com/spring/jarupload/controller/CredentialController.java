package com.spring.jarupload.controller;

import com.spring.jarupload.model.Credentials;
import com.spring.jarupload.services.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CredentialController {

    @Autowired
    CredentialsService credentialsService;

    @PostMapping("/credential")
    @ResponseBody ResponseEntity<?> credential(
            @RequestBody Credentials credentials
    ) {
        credentialsService.store(credentials);
        return ResponseEntity.ok(credentials);
    }
    @GetMapping("/credential/reset")
    void resetCredential(){
        credentialsService.reset();
    }
}
