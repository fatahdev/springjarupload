package com.spring.jarupload.controller;

import com.spring.jarupload.model.FileContent;
import com.spring.jarupload.services.FileContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileContentController {
    @Autowired
    FileContentService contentService;

    @PostMapping("/content")
    @ResponseBody
    ResponseEntity<?> credential(
            @RequestBody FileContent content
    ) {
        contentService.store(content);
        return ResponseEntity.ok(content);
    }
}
