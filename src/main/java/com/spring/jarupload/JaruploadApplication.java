package com.spring.jarupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaruploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaruploadApplication.class, args);
	}

}
