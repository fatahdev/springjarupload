package com.spring.jarupload.services;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public class SshRemoteService {
    private Logger logger = LoggerFactory.getLogger(SshRemoteService.class);

    public void sshExec(String host, String user, String password, String command, String path,String script){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, 22);
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);;
            session.setPassword(password);
            session.connect();

            System.out.println(command +" "+ path);

            String finalCommand = command +" "+ path + "/" + script;

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(finalCommand);

            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream input = channel.getInputStream();
            channel.connect();

            System.out.println("Channel Connected to machine " + host + " server with command: " + finalCommand);

            try {
                InputStreamReader inputReader = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(inputReader);
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                }
                bufferedReader.close();
                inputReader.close();
            } catch(IOException ex) {
                ex.printStackTrace();
            }

            channel.disconnect();
            session.disconnect();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
