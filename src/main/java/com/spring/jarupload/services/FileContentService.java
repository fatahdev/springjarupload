package com.spring.jarupload.services;

import com.mongodb.lang.Nullable;
import com.spring.jarupload.model.FileContent;
import com.spring.jarupload.mongodb.repository.FileContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileContentService {

    @Autowired
    FileContentRepository fileContentRepository;

    public void store(FileContent content){
        fileContentRepository.store(content);
    }
    public FileContent findContentByStatus(boolean status){
        return  fileContentRepository.findContentByStatus(status);
    };
}
