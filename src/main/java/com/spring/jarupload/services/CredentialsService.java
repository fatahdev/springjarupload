package com.spring.jarupload.services;

import com.spring.jarupload.model.Credentials;
import com.spring.jarupload.mongodb.repository.CredentialsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CredentialsService {
    @Autowired
    CredentialsRepository credentialsRepository;

    public void store(Credentials databaseModel) {
        credentialsRepository.store(databaseModel);
    }
    public void reset() {
        credentialsRepository.reset();
    }
    public List<Credentials> findByUploaded(int uploaded) {
        return credentialsRepository.findByUploaded(uploaded);
    }
}
