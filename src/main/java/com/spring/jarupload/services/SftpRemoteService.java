package com.spring.jarupload.services;

import java.io.File;
import java.io.FileInputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SftpRemoteService {
    private ChannelSftp sftpChannel = null;
    private Logger logger = LoggerFactory.getLogger(SftpRemoteService.class);

    private String host;
    private String user;
    private String pass;
    private String remoteDir;

    public SftpRemoteService(String host, String user, String pass, String remoteDir) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.remoteDir = remoteDir;
    }

    private boolean connect() {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(pass);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            this.sftpChannel = (ChannelSftp) channel;

            logger.debug("connected to " + host);
            return sftpChannel.isConnected();
        } catch (Exception e) {
            logger.error("Failed to connect : ", e);
            return false;
        }
    }

    private void reconnect() {
        boolean reconnected = false;
        if (sftpChannel == null || !sftpChannel.isConnected() || sftpChannel.isClosed()) {
            logger.debug("trying to connect/reconnect sftpChannel...");
            do {
                reconnected = this.connect();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    logger.error("interrupt sleep: ", e);
                }
            } while (!reconnected);
        } else {
            logger.debug("Still connected to server");
        }
    }

    public void uploadFile(String folder, File file) throws Exception {
        String newDirectory = this.remoteDir+"/"+folder;

        logger.debug("uploading file " + file.getCanonicalPath());
        this.reconnect();

        sftpChannel.cd(newDirectory);
        sftpChannel.put(new FileInputStream(file), file.getName());

        logger.debug(file.getName() + " uploaded");
    }

    private boolean isBlacklistItem(LsEntry entry) {
        return (entry.getFilename().equals(".") || entry.getFilename().equals(".."));
    }
}
