package com.spring.jarupload.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileContent {
    private String fileName;
    private boolean latest;
}
// curl -X POST http://localhost:37001/content -H "Content-Type: application/json"