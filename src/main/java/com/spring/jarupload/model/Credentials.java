package com.spring.jarupload.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Credentials {
    private String serverIp;
    private String username;
    private String password;
    private String remoteDir;
    private String localDir;
    private String clientName;
    private int uploaded = 0;
}
