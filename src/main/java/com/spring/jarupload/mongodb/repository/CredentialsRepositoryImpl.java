package com.spring.jarupload.mongodb.repository;

import com.spring.jarupload.model.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.Optional;

public class CredentialsRepositoryImpl implements CredentialsRepository {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public <S extends Credentials> S save(S entity) {
        return null;
    }

    @Override
    public <S extends Credentials> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Credentials> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public List<Credentials> findAll() {
        return null;
    }

    @Override
    public Iterable<Credentials> findAllById(Iterable<String> strings) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(Credentials entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Credentials> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Credentials> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Credentials> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Credentials> S insert(S entity) {
        return null;
    }

    @Override
    public <S extends Credentials> List<S> insert(Iterable<S> entities) {
        return null;
    }

    @Override
    public <S extends Credentials> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Credentials> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Credentials> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Credentials> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Credentials> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Credentials> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public void store(Credentials credentials) {
        Query query = new Query(Criteria.where("serverIp").is(credentials.getServerIp()));

        Update update = new Update();
        update.set("serverIp",credentials.getServerIp());
        update.set("username",credentials.getUsername());
        update.set("password",credentials.getPassword());
        update.set("remoteDir",credentials.getRemoteDir());
        update.set("clientName",credentials.getClientName());
        update.set("localDir",credentials.getLocalDir());
        update.set("uploaded", credentials.getUploaded());

        mongoTemplate.upsert(query,update,"credentials");
    }

    @Override
    public void reset() {
        List<Credentials> credentials =  mongoTemplate.findAll(Credentials.class,"credentials");
        for (Credentials credential : credentials){
            credential.setUploaded(0);
            this.store(credential);
        }
    }

    @Override
    public List<Credentials> findByUploaded(int uploaded) {
        Query query = new Query(Criteria.where("uploaded").is(uploaded));
        return mongoTemplate.find(query,Credentials.class,"credentials");
    }
}
