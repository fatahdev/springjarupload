package com.spring.jarupload.mongodb.repository;

import com.spring.jarupload.model.Credentials;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CredentialsRepository extends MongoRepository<Credentials,String> {
    void store(Credentials credentials);
    void reset();
    List<Credentials> findByUploaded(int uploaded);
}
