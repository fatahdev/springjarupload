package com.spring.jarupload.mongodb.repository;

import com.spring.jarupload.model.FileContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.Optional;

public class FileContentRepositoryImpl implements FileContentRepository{
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void store(FileContent content) {

        Query query = new Query(Criteria.where("fileName").is(content.getFileName()));

        Update update = new Update();
        update.set("fileName", content.getFileName());
        update.set("latest", content.isLatest());

        mongoTemplate.upsert(query,update,"content");
    }

    @Override
    public FileContent findContentByStatus(boolean status) {
        Query query = new Query(Criteria.where("latest").is(status));
        return mongoTemplate.findOne(query,FileContent.class,"content");
    }

    @Override
    public <S extends FileContent> S save(S entity) {
        return null;
    }

    @Override
    public <S extends FileContent> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<FileContent> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public List<FileContent> findAll() {
        return null;
    }

    @Override
    public Iterable<FileContent> findAllById(Iterable<String> strings) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(FileContent entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends FileContent> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<FileContent> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<FileContent> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends FileContent> S insert(S entity) {
        return null;
    }

    @Override
    public <S extends FileContent> List<S> insert(Iterable<S> entities) {
        return null;
    }

    @Override
    public <S extends FileContent> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends FileContent> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends FileContent> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends FileContent> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends FileContent> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends FileContent> boolean exists(Example<S> example) {
        return false;
    }
}
