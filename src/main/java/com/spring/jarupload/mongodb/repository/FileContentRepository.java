package com.spring.jarupload.mongodb.repository;

import com.spring.jarupload.model.FileContent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FileContentRepository extends MongoRepository<FileContent,String> {
    void store(FileContent content);
    FileContent findContentByStatus(boolean status);
}
