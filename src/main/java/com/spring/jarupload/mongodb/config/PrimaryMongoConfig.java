package com.spring.jarupload.mongodb.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.spring.jarupload.mongodb.repository", mongoTemplateRef = "primaryMongoTemplate")
 public class PrimaryMongoConfig {

 }