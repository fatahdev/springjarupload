package com.spring.jarupload.mongodb.config;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Configuration
public class MultipleMongoConfig {
    @Primary
    @Bean(name = "primary")
    @ConfigurationProperties(prefix = "primary.db")
    public MongoProperties getPrimary() {
        return new MongoProperties();
    }

    @Autowired 
    MappingMongoConverter converter;

    @Primary
    @Bean(name = "primaryMongoTemplate")
    public MongoTemplate primaryMongoTemplate() throws Exception {
        // List<Converter<?, ?>> converters = new ArrayList<Converter<?, ?>>();
        // converters.add(new Report.ReportReadConverter());
        // converters.add(new Report.ReportWriteConverter());
        // converter.setCustomConversions(new CustomConversions(CustomConversions.StoreConversions.NONE, converters));
        MongoTemplate mongoTemplate = new MongoTemplate(primaryFactory(getPrimary()), converter);
        return mongoTemplate;
    }

    @Primary
    @Bean(name = "primaryFactory")
    public SimpleMongoClientDatabaseFactory primaryFactory(final MongoProperties mongo) throws Exception {
        return new SimpleMongoClientDatabaseFactory(getMongoClient(mongo), mongo.getDatabase());
    }

    private MongoClient getMongoClient(final MongoProperties mongo) {
        return MongoClients.create(
            MongoClientSettings.builder()
                .applyToClusterSettings(builder -> builder
                    .hosts(Arrays.asList(new ServerAddress(mongo.getHost(), mongo.getPort())))
                )
                .applyToConnectionPoolSettings(builder -> builder
                    .minSize(5)
                    .maxSize(100)
                    .maxConnectionLifeTime(120000, TimeUnit.MILLISECONDS)
                    .maxConnectionIdleTime(30000, TimeUnit.MILLISECONDS)
                )
                .applyToSocketSettings(builder -> builder
                    .connectTimeout(30000, TimeUnit.MILLISECONDS)
                    .readTimeout(60000, TimeUnit.MILLISECONDS)
                )
                .credential(MongoCredential.createCredential(mongo.getUsername(), mongo.getDatabase(), mongo.getPassword()))
                .readConcern(ReadConcern.MAJORITY)
                .writeConcern(WriteConcern.ACKNOWLEDGED.withJournal(true))
                .readPreference(ReadPreference.primary())
                .applicationName("JarUpload")
                .compressorList(Arrays.asList(MongoCompressor.createZlibCompressor().withProperty(MongoCompressor.LEVEL, 5)))
                .retryWrites(true)
                .build()
        );
    }
}