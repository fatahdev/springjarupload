package com.spring.jarupload.scheduler;

import com.spring.jarupload.model.Credentials;
import com.spring.jarupload.model.FileContent;
import com.spring.jarupload.services.CredentialsService;
import com.spring.jarupload.services.FileContentService;
import com.spring.jarupload.services.SftpRemoteService;
import com.spring.jarupload.services.SshRemoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
@EnableScheduling
public class Scheduler {

    private Logger logger = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    CredentialsService credentialsService;

    @Autowired
    FileContentService fileContentService;

    @Value("${remote.dir}")
    private String remoteDir;

    @Value("${local.dir}")
    private String localDir;

    @Scheduled(fixedRate = 10000)
    public void checkChange() throws Exception {
        System.out.println("Running");
        List<Credentials> credentialsList = credentialsService.findByUploaded(0);
        if(credentialsList.size() > 0 ) {
            FileContent fileContent = fileContentService.findContentByStatus(true);

            for (Credentials credentials : credentialsList) {
                try {
                    SftpRemoteService sftpRemoteService = new SftpRemoteService(credentials.getServerIp(), credentials.getUsername(), credentials.getPassword(), credentials.getRemoteDir());
                    sftpRemoteService.uploadFile("/newJar/", new File(localDir + "/" + fileContent.getFileName()));
                    sftpRemoteService.uploadFile("/script/", new File(credentials.getLocalDir() + "/" + "upload.sh"));

                    SshRemoteService sshRemoteService = new SshRemoteService();
                    sshRemoteService.sshExec(credentials.getServerIp(),credentials.getUsername(),credentials.getPassword(),"chmod +x", credentials.getRemoteDir()+"/script", "upload.sh");
                    sshRemoteService.sshExec(credentials.getServerIp(),credentials.getUsername(),credentials.getPassword(),".", credentials.getRemoteDir()+"/script", "upload.sh " + credentials.getRemoteDir() + " /newJar/ " + fileContent.getFileName() + " /archive/ ");


                    credentials.setUploaded(1);
                    credentialsService.store(credentials);
                } catch (Exception e) {
                    logger.error("Failed To Host : " + credentials.getServerIp() + " Msg : " + e.getMessage());
                }
            }
        }
    }
}
